# raopt - Milestone 02
#
# Written by Adrian Hammes
# for course WMDK
#
# version v0.1.0
# 2019-05-19 - 00:05
#

import radb
import radb.ast
import radb.parse
from radb.parse import RAParser as enum

# Allowed operators for selections
allowed_ops = [enum.LT, enum.LE, enum.EQ, enum.GE, enum.GT, enum.NE]

#
# Phase 01
#
def followSelectionsAndSplit(node: radb.ast.RelExpr) -> radb.ast.RelExpr:
	if isinstance(node, radb.ast.Select):
		if node.cond.op == enum.AND:
			# found selection to split
			inner_condition = node.cond.inputs[1] # second conditional expression to selection
			outer_condition = node.cond.inputs[0] # first ...
			
			inner_inputs = node.inputs[0] # only one input into select

			inner_select = radb.ast.Select(inner_condition, inner_inputs)
			outer_select = radb.ast.Select(outer_condition, inner_select)

			node = outer_select

			# Test if there is still a nested AND in the new outer selection and perform algorithm once again
			if node.cond.op == enum.AND:
				node = followSelectionsAndSplit(node)

	# Recursively navigate node's inputs, memorize the new trees (if so made) and place them in the node
	newsubs = []
	for sub in node.inputs:
		newsubs.append(followSelectionsAndSplit(sub))
	node.inputs = newsubs

	return node

def rule_break_up_selections(ra):
	return followSelectionsAndSplit(ra)

#
# Phase 02
#
def getStaticConditionAttr(condition: radb.ast.ValExprBinaryOp) -> str:
	# Test if one paramter is static and the other is not
	condA = condition.inputs[0]
	condB = condition.inputs[1]
	condA_isAttr = isinstance(condA, radb.ast.AttrRef)
	condB_isAttr = isinstance(condB, radb.ast.AttrRef)

	if condA_isAttr and not condB_isAttr:
		return condA.name
	if condB_isAttr and not condA_isAttr:
		return condB.name
	
	return "" # empty otherwise (both are attr or both are static values)

def isStaticCondition(condition: radb.ast.ValExprBinaryOp) -> bool:
	return len(getStaticConditionAttr(condition)) > 0

def findRenamings(node: radb.ast.RelExpr) -> dict:
	# Test if renaming
	if isinstance(node, radb.ast.Rename):
		renaming = {}
		renaming[node.relname] = node.inputs[0].rel
		return renaming
	else:
		renamings = {}
		for sub in node.inputs:
			results = findRenamings(sub)
			renamings = {**renamings, **results} # merge two dictionaries since python 3.5
		return renamings

def findRelationsByAttribute(node: radb.ast.RelExpr, dd: object, attr: str) -> str:
	# Look for relations having attr as attribute
	relations = []
	for entry in dd:
		if attr in dd[entry]:
			relations.append(entry)

	# Now test if there are renamings for above relations
	# and add them to the list as well
	renamings = findRenamings(node)
	for renaming in renamings:
		relation = renamings[renaming]
		if relation in relations:
			relations.append(renaming)
			relations.remove(relation) # delete old relation name due to renaming
	
	return relations

def findPushableRelationsForNode(node: radb.ast.RelExpr, dd: object) -> (list, bool):
	# Test if equality selection
	if not isinstance(node, radb.ast.Select):
		return [], False
	if node.cond.op not in allowed_ops:
		return [], False

	# Test if one paramter is static and the other is not
	# and get attribute name of non-static parameter
	attr = getStaticConditionAttr(node.cond)
	if len(attr):
		# If there is at least one relation having said attribute, we may push
		relations = findRelationsByAttribute(node, dd, attr)
		if len(relations):
			return relations, False
	
	# Other possibilty: R1.A = R2.B and R1 and R2 are nested below
	left = node.cond.inputs[0]
	right = node.cond.inputs[1]
	if isinstance(left, radb.ast.AttrRef) and isinstance(right, radb.ast.AttrRef):
		left_relation = left.rel
		right_relation = right.rel
		relations = listBaseRelations(node)

		if left_relation in relations and right_relation in relations:
			return [left_relation, right_relation], True

	return [], False

def pushSingle(node: radb.ast.RelExpr, selection_relations: list, selection_condition: radb.ast.ValExprBinaryOp, dd: object) -> radb.ast.RelExpr:
	# build new selection object if possible (RelRef or Rename)
	if isinstance(node, radb.ast.RelRef) and node.rel in selection_relations:
		return radb.ast.Select(selection_condition, node)
	elif isinstance(node, radb.ast.Rename) and node.relname in selection_relations:
		return radb.ast.Select(selection_condition, node)
	# otherwise
	elif isinstance(node, radb.ast.Select):
		nested = node.inputs[0]
		if isinstance(nested, radb.ast.Select):
			# child is also a selection, test if both relation lists are equally and do not push further
			nested_relations, dummy = findPushableRelationsForNode(nested, dd)
			if set(selection_relations) == set(nested_relations):
				return radb.ast.Select(selection_condition, node)
	
	# Recursion once again
	newsubs = []
	for sub in node.inputs:
		newsubs.append(pushSingle(sub, selection_relations, selection_condition, dd))
	node.inputs = newsubs
	return node

def pushDouble(node: radb.ast.RelExpr, selection_relations: list, selection_condition: radb.ast.ValExprBinaryOp, dd: object) -> (radb.ast.RelExpr, bool):
	# If cross product, we have to decide if we push deeper or not
	if isinstance(node, radb.ast.Cross):
		left = node.inputs[0]
		right = node.inputs[1]

		# simple cross product below, we may place selection there
		if isinstance(left, radb.ast.RelRef) and isinstance(right, radb.ast.RelRef):
			# however only if relations match
			left_rel = left.rel
			right_rel = right.rel
			if left_rel in selection_relations and right_rel in selection_relations:
				return radb.ast.Select(selection_condition, node), True

	# otherwise
	newsubs = []
	placed = False
	for sub in node.inputs:
		newsub, success = pushDouble(sub, selection_relations, selection_condition, dd)
		newsubs.append(newsub)
		if success:
			placed = True
	node.inputs = newsubs
	return node, placed

def followSelectionsAndPush(node: radb.ast.RelExpr, dd: object) -> radb.ast.RelExpr:
	# Recursion - yay!
	newsubs = []
	for sub in node.inputs:
		newsubs.append(followSelectionsAndPush(sub, dd))
	node.inputs = newsubs
	
	# Push selection if possible
	relations, double = findPushableRelationsForNode(node, dd)
	if len(relations):
		if not double:
			node = pushSingle(node, relations, node.cond, dd)
			node = node.inputs[0] # current node is selection to be omitted
		else:
			node, success = pushDouble(node, relations, node.cond, dd)
			if success:
				node = node.inputs[0] 
	
	return node

def rule_push_down_selections(ra1, dd):
	# TODO:
	# 1. find selection
	# 2. search base relations in selection's inputs pre-order recursively
	# 3a. push deepest selection first down to base relation
	# 3b. then push upper selections down to deepest selection
	# 3c. in case we have something like R1.a = R2.b then push down to cross product containing R1 and R2 directly
	return followSelectionsAndPush(ra1, dd)

#
# Phase 03
#
def followSelectionsAndMerge(node: radb.ast.RelExpr) -> radb.ast.RelExpr:
	# outermost node in a tree does not have any further inputs
	if len(node.inputs) == 0:
		return node
	
	nested = node.inputs[0]

	# return immediately if node and its child are not selections after performing recursion on node's inputs
	if not (isinstance(node, radb.ast.Select) and isinstance(nested, radb.ast.Select)):
		newchilds = []
		
		for child in node.inputs:
			newchilds.append(followSelectionsAndMerge(child))
		node.inputs = newchilds

		return node

	# recursively find nested selects and merge them if possible
	# now node and nested are the only remaining selections (in this node)
	nested = followSelectionsAndMerge(nested)

	outer_cond = node.cond
	inner_cond = nested.cond

	cond = radb.ast.ValExprBinaryOp(outer_cond, enum.AND, inner_cond)
	node = radb.ast.Select(cond, nested.inputs[0])

	return node

def rule_merge_selections(ra2):
	# TODO:
	# 1. Recursively post-orderly find nested selects and merge two deepest
	# 1. Push them up and redo step #1 again
	# 3. Finish if last selection is merged
	return followSelectionsAndMerge(ra2)

#
# Phase 04
#
def listBaseRelations(node: radb.ast.RelExpr) -> list:
	# Already a RelRef or Rename, return name immediately
	if isinstance(node, radb.ast.RelRef):
		return [node.rel]
	elif isinstance(node, radb.ast.Rename):
		return [node.relname]

	# Otherwise check all inputs recursively
	relations = []
	for subnode in node.inputs:
		results = listBaseRelations(subnode)
		for result in results:
			relations.append(result)

	return relations

def listConditions(cond: radb.ast.ValExprBinaryOp) -> (list, list):
	if cond.op == enum.AND:
		left0, right0 = listConditions(cond.inputs[0])
		left1, right1 = listConditions(cond.inputs[1])
		return left0 + left1, right0 + right1
	else:
		left = cond.inputs[0]
		right = cond.inputs[1]
		if right.rel < left.rel:
			temp = right
			right = left
			left = temp
		return  [left.rel], [right.rel]

def testNodeJoinability(node: radb.ast.RelExpr) -> bool:
	# 1 - check if equality selection
	if not isinstance(node, radb.ast.Select):
		return False
	ls = allowed_ops + [enum.AND]
	if node.cond.op not in ls:
		return False
	selection = node
	
	# 2 - check if cross product as input
	child = selection.inputs[0]
	if not isinstance(child, radb.ast.Cross):
		return False
	crossproduct = child
	
	# 3 - check if attributes from left and right are compared within selection
	left = crossproduct.inputs[0]
	right = crossproduct.inputs[1]

	left_relations = listBaseRelations(left)
	right_relations = listBaseRelations(right)
	
	# test if condition consists of multiple sub-conditions
	left_cond_rels, right_cond_rels = listConditions(node.cond)

	# Final test
	if (set(left_cond_rels) <= set(left_relations) and set(right_cond_rels) <= set(right_relations)) or (set(left_cond_rels) <= set(right_relations) and set(right_cond_rels) <= set(left_relations)):
		return True
	return False

def followSelectionsAndJoin(node: radb.ast.RelExpr) -> radb.ast.RelExpr:
	# Recursion - boom!
	newsubs = []
	for sub in node.inputs:
		newsubs.append(followSelectionsAndJoin(sub))
	node.inputs = newsubs

	# Now introduce join if possible
	if testNodeJoinability(node):
		# 4 - do the join
		child = node.inputs[0]

		R1 = child.inputs[0]
		R2 = child.inputs[1]

		cond = node.cond
		# Test for special condition XY.SCS-2019:107/B "top secret"
		# (condition inputs get swapped if they are concatinated using AND operator)
		if cond.op == enum.AND:
			temp = cond.inputs[0]
			cond.inputs[0] = cond.inputs[1]
			cond.inputs[1] = temp
		
		join = radb.ast.Join(R1, cond, R2)
		node = join

	return node

def rule_introduce_joins(ra3):
	# TODO:
	# 1. check if equality selection
	# 2. check if cross product as input
	# 3. check if attributes from R1 and R2 are compared within selection
	# 4. build join using conditions
	return followSelectionsAndJoin(ra3)
